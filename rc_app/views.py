import datetime

from django.http.response import JsonResponse
from django.shortcuts import render
from win32com.client import Constants

import rc_app
from rc_app import Constants, truedata, PredictionHelper, SignalHelper
from rc_app.models import Prediction, LiveSignal

list_csv = "NIFTY-I,ACC-I,ADANIPORTS-I,AMBUJACEM-I,ASIANPAINT-I,AUROPHARMA-I,AXISBANK-I,BAJAJ-AUTO-I,BANKBARODA-I,BHEL-I,BPCL-I,BHARTIARTL-I,INFRATEL-I,BOSCHLTD-I,CIPLA-I,COALINDIA-I,DRREDDY-I,EICHERMOT-I,GAIL-I,GRASIM-I,HCLTECH-I,HDFCBANK-I,HEROMOTOCO-I,HINDALCO-I,HINDUNILVR-I,HDFC-I,ITC-I,ICICIBANK-I,IDEA-I,INDUSINDBK-I,INFY-I,KOTAKBANK-I,LT-I,LUPIN-I,M&M-I,MARUTI-I,NTPC-I,ONGC-I,POWERGRID-I,RELIANCE-I,SBIN-I,SUNPHARMA-I,TCS-I,TATAMTRDVR-I,TATAMOTORS-I,TATAPOWER-I,TATASTEEL-I,TECHM-I,ULTRACEMCO-I,WIPRO-I,YESBANK-I,ZEEL-I"
list_csv_split = list_csv.split(",")

def run(request):
    truedata.getLiveDataAndInsertIntoDB('NIFTY-I','0930')
    return JsonResponse({'test':'Ok'})


def signal_stock0930(request):
    SignalHelper.publish_signal0930(Constants.StockContants.SUBSCRIBE_LIST,None,None)
    return JsonResponse({"test": "dont know"})


def signal_stock(request):
    SignalHelper.publish_signal(Constants.StockContants.SUBSCRIBE_LIST,None,None)
    return JsonResponse({"test": "Ok"})

def prediction(request):
    predict_obj = Prediction.objects.filter(symbol__in=(list_csv_split)).order_by('-update_time')#[:len(list_csv_split)]
    return render(request,"getsignal.html", {"table": predict_obj})

def signal(request):
    predict_obj = LiveSignal.objects.filter(symbol__in=(list_csv_split)).order_by('-update_time')#[:len(list_csv_split)]
    #predict_obj = Prediction.objects.raw('SELECT TOP '  +len(list_csv_split) + ' * FROM PREDICTION WHERE ' )
    # return render(request,"getsignal.html", {"table": predict_obj})
    print(rc_app.test1.zk.cmdSubscribe('NIFTY-I'))
    return JsonResponse({"test": "Ok"})


def home(request):
    #predict.predictAndPublish('NIFTY-I','0930')

    data = Prediction.objects.all()
    ret = {}
    ret_dict = []

    for x in data:
        symbol = x.symbol
        target1 =x.target1
        target2 = x.target2
        target3 = x.target3
        stoploss = x.stoploss
        signal = x.prediction
        update_time = x.update_time
        popup_string =signal + " " + symbol +" on Targets: \n" + str(target1)+ "\n" + str(target2) + "\n" + str(target3 )+"\n" + "Keep Stop Loss on : " +str(stoploss)
        record = {'symbol':symbol,'target1':target1,"target2":target2,"target3":target3,"stoploss":stoploss,"signal":signal,
                  'update_time':str(update_time),"popup_string":popup_string}
        ret_dict.append(record)

    ret["Cards"]=ret_dict
    return JsonResponse(ret)

def index(request):
    data = Prediction.objects.all()
    return render(request, Constants.TemplateConstants.current_template, {'data': data})


def predict_stock(request):
    PredictionHelper.publish_prediction(list_csv,'NSE','DERIVATIVE')
    return JsonResponse({'Success': 'Yes'})


def get_stock(request):
    #Logic to fetch latest prediction for the given stock, make JSON out of it an
    current_date = datetime.datetime.now()
    resp = None
    #need an alternative method tofind out whether to return prediction or signal based on Holidays etc.
    weekday = current_date.weekday()
    if (current_date.hour >= 18 and current_date.minute >=  30) or (weekday == 5 or weekday == 6):
        resp = PredictionHelper.getPredictionJSON()
        isPred = True
    else:
        resp = SignalHelper.getSignalJSON()

    return JsonResponse(resp)


