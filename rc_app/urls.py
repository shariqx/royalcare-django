"""rc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin

import rc_app
from rc_app import views, settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', views.home),
    url(r'^predict_stock/', views.predict_stock),
    url(r'^get_stock/', views.get_stock),
    url(r'^signal_stock/',views.signal_stock),
url(r'^signal_stock0930/',views.signal_stock0930),
    url(r'^signal/',views.signal),
url(r'^prediction/',views.prediction),
    url(r'^index/', views.index),
]

