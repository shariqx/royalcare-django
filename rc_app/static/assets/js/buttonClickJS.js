$(document).ready(function(){
    $("equity_predictions").click(function(){
    	$.ajax({url: "RequestHandler", 
        	data : {open: openval,
        			high : highval, 
        			low : lowval, 
        			close : closeval
        			},
        			dataType: "json",
        	success: function(result)
        	{
          /*  var DecisionResult  = JSON.parse(result);*/
            $("#decision").html(
            		'<br><br>' + 'Decision : ' +  result.Decision + '<br>' + 
            		'Target 1 : ' +  result.Target1+ '<br>' + 
            		'Target 2: ' +  result.Target2+ '<br>' + 
            		'Target 3 : ' +  result.Target3 + '<br>'  + 
            		'Stop Loss :'  + result.StopLoss); 
        }});
    });
});

function foo(symbolname) 
{
    	selectedDerivative = $('#select_derivatives').val();
    	$.ajax({url: "RequestHandler", 
        	data : {
        			trigger : symbolname,
        			symbol : selectedDerivative
        	},
        			dataType: "json",
        			
        	success: function(result)
        	{
          /*  var DecisionResult  = JSON.parse(result);*/
            $("#decision").html(
            		'<br><br>' + 'Decision : ' +  result.Decision + '<br>' + 
            		'Target 1 : ' +  result.Target1+ '<br>' + 
            		'Target 2: ' +  result.Target2+ '<br>' + 
            		'Target 3 : ' +  result.Target3 + '<br>'  + 
            		'Stop Loss :'  + result.StopLoss); 
        }});
}