import datetime
from _decimal import Decimal

from rc_app import truedata, Constants
from rc_app.models import Prediction
from rc_app.predict import calculateTargetAndStopLoss


def publish_prediction(stock_list,exchange,type):
    stock_dict = getEODDataForPrediction(stock_list)
    stock_csvsplit = stock_list.split(",")

    insert_dict  = dict()
    for stock in stock_csvsplit:
        open = stock_dict[stock + '.OPEN']
        high = stock_dict[stock + '.HIGH']
        low = stock_dict[stock + '.LOW']
        close = stock_dict[stock + '.CLOSE']
        atp = stock_dict[stock + '.ATP']
        predict = getPredictionDict(open,high,low,close,atp)
        predict['EXCHANGE'] =  exchange
        predict['TYPE'] = type
        insert_dict[stock] = predict

    insertDictIntoPrediction(insert_dict)



def getEODDataForPrediction(stock_list):
    print('@@@ Inside getEODDataForPrediction @@@')
    zk = truedata.dispatch()
    truedata.login(zk)
    snap_data = zk.cmdSubscribe(stock_list)
    snap_dict = truedata.getATPFromSnapDataResp(snap_data)
    stock_csvsplit = stock_list.split(",")

    for stock in stock_csvsplit:
        #get previous day data, make sure to run it after the market has ended and before the market starts.
        eod_resp = zk.cmdEodNumDays(stock,'1')
        eod_resp_split = eod_resp.split(",")
        snap_dict[stock+'.OPEN'] = Decimal(eod_resp_split[1])
        snap_dict[stock + '.HIGH'] = Decimal(eod_resp_split[2])
        snap_dict[stock + '.LOW'] = Decimal(eod_resp_split[3])
        snap_dict[stock + '.CLOSE'] = Decimal(eod_resp_split[4])

    print('@@@ getEODDATaForPrediction :: Found All EOD values, returning.... @@@')
    #zk.cmdLogout()
    return snap_dict

def getPredictionDict(open,high,low,close,atp):
    prediction = ""

    if close > atp :
        prediction = "BUY"
    else:
        prediction = "SELL"

    prediction_dict = calculateTargetAndStopLoss(open,high,low,close,prediction)
    return prediction_dict



def insertDictIntoPrediction(insert_dict):

    for element in insert_dict:
    #Each element is a stock, which contains Prediction object that has to be inserted into the database
        symbol = element
        data = insert_dict[element]

        lp = Prediction(
            symbol = symbol,
            exchange = data['EXCHANGE'],
            target1 = data['TARGET1'],
            target2 = data['TARGET2'],
            target3 = data['TARGET3'],
            stoploss = data['STOPLOSS'],
            update_time = datetime.datetime.now(),
            type = data['TYPE'],
            update_by = 'PREDICTION',
            prediction = data['PREDICTION']
        )
        lp.save()

def getPredictionJSON():
    symbol_list = Constants.StockContants.SUBSCRIBE_LIST.split(",")
    predict_obj = Prediction.objects.filter(symbol__in=(symbol_list))\
    .order_by('-update_time')[:len(symbol_list)]
    stock_jsonarr = []
    for row in predict_obj:
        symbol = row.symbol
        target1 = row.target1
        target2 = row.target2
        target3 = row.target3
        stoploss = row.stoploss
        update_time = row.update_time
        prediction = row.prediction
        record = {"Symbol": symbol, "Current": 0.0, "Prediction": prediction, "Target1": str(target1),
                  "Target2": str(target2), "Target3": str(target3), "Stoploss": str(stoploss),
                  "Datetime": str(update_time), "Type":"prediction"}
        print(record)
        stock_jsonarr.append(record)

        # pickup_records = json.dumps(pickup_records)
    # pickup_records = json.dumps(stock_jsonarr)
    pickup_response = {"Stocks": stock_jsonarr}
    return pickup_response