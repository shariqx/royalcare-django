import datetime

from rc_app import truedata, Constants, predict
from rc_app.models import LiveSignal


def publish_signal0930(stock_list,exchange,type):
    print('@@@ Inside publish_signal  0930@@@')
    #OHLC be same as last day, but take close of 0930 instead of open of 0930....
    close_dict = truedata.getOHLCFor0930(stock_list)
    ohlc_dict = truedata.getLastEODsDict(stock_list)
    main_predict = dict()
    for stock in stock_list.split(","):
        print('For symbol : ' +stock)
        curr_dict = ohlc_dict[stock]
        open = curr_dict['OPEN']
        prev_high = curr_dict['HIGH']
        prev_low = curr_dict['LOW']
        prev_close = curr_dict['CLOSE']
        open = close_dict[stock]['CLOSE']
        preDict = predict.calculate_signal(prev_high,prev_low,prev_close,open,'0930')
        preDict['SYMBOL'] = stock
        main_predict[stock] = preDict
        insertSignalIntoDB(main_predict)

#Here, we need to take OHLC of 9:30, and then everything is the same,
#   Except, need to consider close instead of open for searching in the list.

def publish_signal(stock_list,exchange,type):
    stock_list = 'NIFTY-I'
    open_dict = truedata.get_dict_from_Tlines_resp(stock_list)
    ohlc_dict = truedata.getLastEODsDict(stock_list)
    main_predict = dict()
    for stock in stock_list.split(","):
        print('For symbol : ' + stock)
        curr_dict = open_dict[stock]
        open = curr_dict['OPEN']
        curr_dict = ohlc_dict[stock]
        prev_open = curr_dict['OPEN']
        prev_high = curr_dict['HIGH']
        prev_low = curr_dict['LOW']
        prev_close = curr_dict['CLOSE']
        preDict = predict.calculate_signal(prev_high,prev_low,prev_close,open,'0900')
        preDict['SYMBOL'] = stock
        main_predict[stock] = preDict
        insertSignalIntoDB(main_predict)



def   insertSignalIntoDB(main_predict):
    for  entry in main_predict:
        stock  = main_predict[entry]
        signal_entry = LiveSignal(
            symbol = stock['SYMBOL'],
            buy_above = stock['BUY_ABOVE'],
            sell_below = stock['SELL_BELOW'],
            btarget1 = stock['BTARGET1'],
            btarget2 = stock['BTARGET2'],
            btarget3 = stock['BTARGET3'],
            bstoploss = stock['BSTOPLOSS'],
            starget1=stock['STARGET1'],
            starget2=stock['STARGET2'],
            starget3=stock['STARGET3'],
            sstoploss=stock['SSTOPLOSS'],
            update_time = datetime.datetime.now(),
            created_by = 'SIGNAL'
        )
        signal_entry.save()


def getSignalJSON():
    symbol_list = Constants.StockContants.SUBSCRIBE_LIST.split(",")
    predict_obj = LiveSignal.objects.filter(symbol__in=(symbol_list)).order_by('-update_time')[:len(symbol_list)]
    stock_jsonarr = []
    for row in predict_obj:
        symbol = row.symbol
        buy_above = row.buy_above
        btarget1 = row.btarget1
        btarget2 = row.btarget2
        btarget3 = row.btarget3
        bstoploss = row.bstoploss
        sell_below = row.sell_below
        starget1 = row.starget1
        starget2 = row.starget2
        starget3 = row.starget3
        sstoploss = row.sstoploss
        update_time = row.update_time

        record = {"Symbol": symbol, "Current": 0.0, "Buy_Above": str(buy_above),
                  "BTarget1": str(btarget1),"BTarget2": str(btarget2),
                  "BTarget3": str(btarget3),
                  "BStoploss": str(bstoploss),
                  "Sell_Below": str(sell_below),
                  "STarget1": str(starget1), "STarget2": str(starget2),
                  "STarget3": str(starget3),
                  "SStoploss": str(sstoploss),
                  "Datetime": str(update_time),
                  "Type" : "Signal"
                  }
        print(record)
        stock_jsonarr.append(record)

        # pickup_records = json.dumps(pickup_records)
    # pickup_records = json.dumps(stock_jsonarr)
    pickup_response = {"Stocks": stock_jsonarr}
    return pickup_response