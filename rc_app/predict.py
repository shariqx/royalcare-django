import datetime
from _decimal import Decimal, getcontext

from array import array

import numpy

from rc_app.models import MarketData, Prediction
from rc_app import Constants, truedata


#Predict using the topmost record in the DB for the particular symbol and publish it to predictions table.


def predictAndPublish(symbol, update_by):
    retdict = getCurrentMDataForSymbol(symbol)
    if update_by == Constants.FormulaConstants.INTRA_NINETHIRTY_ATP_BASED:
        #for preDict in retdict:
        publish0930(retdict,update_by)
    if update_by == '0300':
        curr_pred = getCurrentMDataForSymbol(symbol)
        publish0300(retdict,curr_pred,'0300')


def getCurrentMDataForSymbol(symbol):
    returnme = dict()
    object = MarketData.objects.all().filter(symbol=symbol).order_by('-update_time')
    return object[0]


def getCurrentPredictionForSymbol(symbol):
    returnme = dict()
    object = Prediction.objects.all().filter(symbol = symbol).order_by('-update_time')
    return object[0]

def publish0930(dict1,update_by):
    #get prev day OHLC
    ohlcdict = truedata.getLastEODsDict(dict1.symbol)
    open = Decimal(ohlcdict['open'])
    close = Decimal(ohlcdict['close'])
    prev_close = dict1.prev_close
    high = Decimal(ohlcdict['high'])
    low = Decimal(ohlcdict['low'])
    atp = Decimal(dict1.atp0930)
    preDict = dict()

    if atp > open:
        decision = 'BUY'
        preDict = calculateTargetAndStopLoss(open,high,low,close,prev_close,decision)
    if atp < open:
        decision = 'SELL'
        preDict = calculateTargetAndStopLoss(open,high,low,close,prev_close,decision)
    preDict['update_by'] = update_by
    insertPreDictIntoDB(dict1.symbol,preDict,decision)

def publish0300(prev_dict,livepred_dict,update_by):
    #Algorithm:
    #Get current Open,High,Low,ATP(Just-In-Case) from curr_dict
    #Get Prev -//- from prev_dict
    #If Current Open > 0930 ATP then predict BUY and call calculateTargetAndStopLoss
    #return the targets & stop losses.
    ohlcdict = truedata.getLastEODsDict(prev_dict.symbol)
    open = Decimal(ohlcdict['open'])
    close = Decimal(ohlcdict['close'])
    prev_close = prev_dict.prev_close
    high = Decimal(ohlcdict['high'])
    low = Decimal(ohlcdict['low'])
    atp = Decimal(prev_dict.atp0930)
    preDict = dict()

    curr_dict  = truedata.getSnapDataForSymbol(prev_dict.symbol)
    curr_rate  = curr_dict[prev_dict.symbol + '.LTP']
    print('current rate at 3:00 --' + curr_rate)
    print('09:30 ATP is -- '+ str(prev_dict.atp0930))

    decision = ''
    if Decimal(curr_rate)> prev_dict.atp0930:
        decision = 'BUY'
    else:
        decision ='SELL'
    print('Hence the decision is -- ' + decision)
    preDict = calculateTargetAndStopLoss(open, high, low, close, prev_close, decision)
    #get current targets and stop loss from 0930 livepred.
    preDict['update_by'] = update_by
    #TODO - Find out which data is to be inserted into the DB for 0300 singal.
    insertPreDictIntoDB(prev_dict.symbol, preDict,decision)

def insertPreDictIntoDB(symbol,preDict,prediction):
    lp = Prediction(
        prediction = prediction,
        target1  = preDict['T1'],
        target2  =  preDict['T2'],
        target3 =preDict['T3'],
        stoploss = preDict['stoploss'],
        update_time = datetime.datetime.now(),
        symbol = symbol,
        update_by  = preDict['update_by'],
     )
    lp.save()


def calculateTargetAndStopLoss(open,high,low,close,decision):
    preDict = dict()
    stoploss = 0.00

    if decision == 'BUY':
        close = Decimal(close)
        T1 = ((high - low) * (Decimal(1.1/4))) + close
        T2 = ((high - low) * (Decimal(1.1/2))) + close
        T3 = ((high - low) * (Decimal(1.1/1.4))) + close
        stoploss = getStopLossBuy_H4(open,high,low,close)

    if decision =='SELL':
        T1 = close -Decimal(((high-low)*(Decimal(1.1/4))))
        T2 = close-Decimal(((high-low)*(Decimal(1.1/2))))
        T3 = close-Decimal(((high-low)*(Decimal(1.1/1.41))))
        stoploss = getStopLossSell_L4(open, high, low, close)


    preDict['TARGET1'] = T1
    preDict['TARGET2'] = T2
    preDict['TARGET3'] = T3
    preDict['STOPLOSS'] = stoploss
    preDict['PREDICTION'] = decision
    return preDict


def getStopLossBuy_H4(open,high,low,close):
    return close-((high-low)*(Decimal(1.1/2)))

def getStopLossSell_L4(open,high,low,close):
    return ((high-low)*(Decimal(1.1//2)))+close


def calculate_signal(prev_high,prev_low,prev_close,curr_open,created_by):
    #Algorithm:
    #   (i) Calculate L6-H6, R3-S1.
    #   (ii) Find out where the open lies between L6-H6, e.g if it lies b/w L1-H1, call L1 X and H1 Y.
    #   (iii) Value below X is sell below and above Y is buy above(here, L2 would be sell below and H2 would be buy above
    #   (iv) For buy targets, Merge values above Buy_above with R1,R2,R3 and put them in ascending order.
    #   (v)  For sel targets, Merge values below Sell_below with S1, S2,S3 and put them in descending order.
    #   (vi) Now the 3 values above Buy_above are buy targets and so below Sell_below is sell targets
    #   (v) Buy_above is stock loss for sell, and sell_above is stop loss for buy.

    #(i) Calculate L6-H6, R3-S1
    #getcontext().prec = 6
    arr = array('d')
    prev_high = Decimal(prev_high)
    prev_low = Decimal(prev_low)
    prev_close = Decimal(prev_close)
    curr_open = Decimal(curr_open)

    L1 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 12)))
    L2 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 6)))
    L3 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 4)))
    L4 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 2)))
    L5 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 1.41)))
    L6 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 1.09)))

    H1 = ((prev_high - prev_low) * Decimal((1.1 / 12))) + prev_close
    H2 = ((prev_high - prev_low) * Decimal((1.1 / 6))) + prev_close
    H3 = ((prev_high - prev_low) * Decimal((1.1 / 4))) + prev_close
    H4 = ((prev_high - prev_low) * Decimal((1.1 / 2))) + prev_close
    H5 = ((prev_high - prev_low) * Decimal((1.1 / 1.41))) + prev_close
    H6 = ((prev_high - prev_low) * Decimal((1.1 / 1.09))) + prev_close

    arr.extend((H6, H5, H4, H3, H2, H1, L1, L2, L3, L4, L5, L6))
    arr.reverse()
    buy_above_index = 0
    sell_below_index = 0

    # if created_by == '0930':
    #     curr_open = prev_close

    for i in range(0,10):
        #print( str(curr_open) + ' arr[i] : ' + str(arr[i]))
        if(curr_open >= arr[i] and curr_open <= arr[i+1]):
            buy_above_index = i+2
            sell_below_index = i-1
            break

    retDict = dict();
    buy_above_val = arr[buy_above_index]
    sell_below_val = arr[sell_below_index]

    retDict['BUY_ABOVE'] = buy_above_val
    retDict['SELL_BELOW'] = sell_below_val

    merge_buy = False
    merge_sell = False
    if buy_above_index + 3 <= len(arr)-1 :
        merge_buy = True
        retDict['BTARGET1'] = arr[buy_above_index + 1]
        retDict['BTARGET2'] = arr[buy_above_index + 2]
        retDict['BTARGET3'] = arr[buy_above_index + 3]
        retDict['BSTOPLOSS'] = arr[sell_below_index]

    if sell_below_index - 3  >= 0:
        merge_sell = True
        retDict['STARGET1'] = arr[sell_below_index - 1]
        retDict['STARGET2'] = arr[sell_below_index - 2]
        retDict['STARGET3'] = arr[sell_below_index - 3]
        retDict['SSTOPLOSS'] = arr[buy_above_index]


    if not merge_buy:
        print(' @@@ PREDICTION_HELPER @@@ PLEASE CHECK STOCK')
        DTP = (prev_high+prev_low+prev_close)/3
        S1 = 2*DTP-prev_high
        R1 = 2 * DTP - prev_low
        S2 = DTP-(R1-S1)
        R2 = DTP + (R1 - S1)
        S3 = prev_low-2*(prev_high-DTP)
        R3 = prev_high+2*(DTP-prev_low)

        r_arr = array('d')
        r_arr.extend([R1,R2,R3])

        buy_targets = array('d')
        buy_targets.extend(arr[buy_above_index:len(arr)])
        buy_targets.extend(r_arr)
        buy_targets_sorted = numpy.array(buy_targets)

        #Should be sorted in ascending order, I think.
        buy_targets_sorted.sort()
        #buy_targets_sorted[::-1].sort()

        btarget_counter = 0

        target_counter = 0
        for x in buy_targets_sorted:
            if x > buy_above_val and target_counter  < 3 :
                retDict['BTARGET' + str((target_counter +1))]= x
                target_counter += 1
                btarget_counter+=1

        if target_counter < 3 :
            for i in range(3 - target_counter,0,-1):
                btarget_counter+=1
                retDict['BTARGET'+str(btarget_counter)] =  0.0

        # for x in range(3,btarget_counter,-1):
        #     retDict['BTARGET' + str(x)]

        # retDict['BTARGET1'] = buy_targets_sorted[0]
        # retDict['BTARGET2'] = buy_targets_sorted[1]
        # retDict['BTARGET3'] = buy_targets_sorted[2]
        retDict['BSTOPLOSS'] = sell_below_val

    # Predict using the topmost record in the DB for the particular symbol and publish it to predictions table.
    if not merge_sell:
        print(' @@@ PREDICTION_HELPER @@@ PLEASE CHECK STOCK')
        DTP = (prev_high + prev_low + prev_close) / 3
        S1 = 2 * DTP - prev_high
        R1 = 2 * DTP - prev_low
        S2 = DTP - (R1 - S1)
        R2 = DTP + (R1 - S1)
        S3 = prev_low - 2 * (prev_high - DTP)
        # R3 = prev_high+2*(DTP-prev_low)

        s_arr = array('d')
        s_arr.extend([S1, S2, S3])

        sell_targets = array('d')
        sell_targets.extend(arr[sell_below_index:0])
        sell_targets.extend(s_arr)
        sell_targets_sorted = numpy.array(sell_targets)

        # Should be sorted in ascending order, I think.
        sell_targets_sorted.sort()
        # buy_targets_sorted[::-1].sort()

        btarget_counter = 0

        target_counter = 0
        for x in sell_targets_sorted:
            if x < sell_below_val and target_counter < 3:
                retDict['STARGET' + str((target_counter + 1))] = x
                target_counter += 1
                btarget_counter += 1

        if target_counter < 3:
            for i in range(3 - target_counter, 0, -1):
                btarget_counter += 1
                retDict['STARGET' + str(btarget_counter)] = 0.0

        retDict['SSTOPLOSS'] = sell_below_val
    return retDict

    def predictAndPublish(symbol, update_by):
        retdict = getCurrentMDataForSymbol(symbol)
        if update_by == Constants.FormulaConstants.INTRA_NINETHIRTY_ATP_BASED:
            # for preDict in retdict:
            publish0930(retdict, update_by)
        if update_by == '0300':
            curr_pred = getCurrentMDataForSymbol(symbol)
            publish0300(retdict, curr_pred, '0300')

    def getCurrentMDataForSymbol(symbol):
        returnme = dict()
        object = MarketData.objects.all().filter(symbol=symbol).order_by('-update_time')
        return object[0]

    def getCurrentPredictionForSymbol(symbol):
        returnme = dict()
        object = Prediction.objects.all().filter(symbol=symbol).order_by('-update_time')
        return object[0]

    def publish0930(dict1, update_by):
        # get prev day OHLC
        ohlcdict = truedata.getLastEODsDict(dict1.symbol)
        open = Decimal(ohlcdict['open'])
        close = Decimal(ohlcdict['close'])
        prev_close = dict1.prev_close
        high = Decimal(ohlcdict['high'])
        low = Decimal(ohlcdict['low'])
        atp = Decimal(dict1.atp0930)
        preDict = dict()

        if atp > open:
            decision = 'BUY'
            preDict = calculateTargetAndStopLoss(open, high, low, close, prev_close, decision)
        if atp < open:
            decision = 'SELL'
            preDict = calculateTargetAndStopLoss(open, high, low, close, prev_close, decision)
        preDict['update_by'] = update_by
        insertPreDictIntoDB(dict1.symbol, preDict, decision)

    def publish0300(prev_dict, livepred_dict, update_by):
        # Algorithm:
        # Get current Open,High,Low,ATP(Just-In-Case) from curr_dict
        # Get Prev -//- from prev_dict
        # If Current Open > 0930 ATP then predict BUY and call calculateTargetAndStopLoss
        # return the targets & stop losses.
        ohlcdict = truedata.getLastEODsDict(prev_dict.symbol)
        open = Decimal(ohlcdict['open'])
        close = Decimal(ohlcdict['close'])
        prev_close = prev_dict.prev_close
        high = Decimal(ohlcdict['high'])
        low = Decimal(ohlcdict['low'])
        atp = Decimal(prev_dict.atp0930)
        preDict = dict()

        curr_dict = truedata.getSnapDataForSymbol(prev_dict.symbol)
        curr_rate = curr_dict[prev_dict.symbol + '.LTP']
        print('current rate at 3:00 --' + curr_rate)
        print('09:30 ATP is -- ' + str(prev_dict.atp0930))

        decision = ''
        if Decimal(curr_rate) > prev_dict.atp0930:
            decision = 'BUY'
        else:
            decision = 'SELL'
        print('Hence the decision is -- ' + decision)
        preDict = calculateTargetAndStopLoss(open, high, low, close, prev_close, decision)
        # get current targets and stop loss from 0930 livepred.
        preDict['update_by'] = update_by
        # TODO - Find out which data is to be inserted into the DB for 0300 singal.
        insertPreDictIntoDB(prev_dict.symbol, preDict, decision)

    def insertPreDictIntoDB(symbol, preDict, prediction):
        lp = Prediction(
            prediction=prediction,
            target1=preDict['T1'],
            target2=preDict['T2'],
            target3=preDict['T3'],
            stoploss=preDict['stoploss'],
            update_time=datetime.datetime.now(),
            symbol=symbol,
            update_by=preDict['update_by'],
        )
        lp.save()

    def calculateTargetAndStopLoss(open, high, low, close, decision):
        preDict = dict()
        stoploss = 0.00

        if decision == 'BUY':
            close = Decimal(close)
            T1 = ((high - low) * (Decimal(1.1 / 4))) + close
            T2 = ((high - low) * (Decimal(1.1 / 2))) + close
            T3 = ((high - low) * (Decimal(1.1 / 1.4))) + close
            stoploss = getStopLossBuy_H4(open, high, low, close)

        if decision == 'SELL':
            T1 = close - Decimal(((high - low) * (Decimal(1.1 / 4))))
            T2 = close - Decimal(((high - low) * (Decimal(1.1 / 2))))
            T3 = close - Decimal(((high - low) * (Decimal(1.1 / 1.41))))
            stoploss = getStopLossSell_L4(open, high, low, close)

        preDict['TARGET1'] = T1
        preDict['TARGET2'] = T2
        preDict['TARGET3'] = T3
        preDict['STOPLOSS'] = stoploss
        preDict['PREDICTION'] = decision
        return preDict

    def getStopLossBuy_H4(open, high, low, close):
        return close - ((high - low) * (Decimal(1.1 / 2)))

    def getStopLossSell_L4(open, high, low, close):
        return ((high - low) * (Decimal(1.1 // 2))) + close

    def calculate_signal(prev_open, prev_high, prev_low, prev_close, curr_open):
        # Algorithm:
        #   (i) Calculate L6-H6, R3-S1.
        #   (ii) Find out where the open lies between L6-H6, e.g if it lies b/w L1-H1, call L1 X and H1 Y.
        #   (iii) Value below X is sell below and above Y is buy above(here, L2 would be sell below and H2 would be buy above
        #   (iv) For buy targets, Merge values above Buy_above with R1,R2,R3 and put them in ascending order.
        #   (v)  For sel targets, Merge values below Sell_below with S1, S2,S3 and put them in descending order.
        #   (vi) Now the 3 values above Buy_above are buy targets and so below Sell_below is sell targets
        #   (v) Buy_above is stock loss for sell, and sell_above is stop loss for buy.

        # (i) Calculate L6-H6, R3-S1
        # getcontext().prec = 6
        arr = array('d')
        prev_high = Decimal(prev_high)
        prev_low = Decimal(prev_low)
        prev_close = Decimal(prev_close)
        curr_open = Decimal(curr_open)

        L1 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 12)))
        L2 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 6)))
        L3 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 4)))
        L4 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 2)))
        L5 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 1.41)))
        L6 = prev_close - ((prev_high - prev_low) * Decimal((1.1 / 1.09)))

        H1 = ((prev_high - prev_low) * Decimal((1.1 / 12))) + prev_close
        H2 = ((prev_high - prev_low) * Decimal((1.1 / 6))) + prev_close
        H3 = ((prev_high - prev_low) * Decimal((1.1 / 4))) + prev_close
        H4 = ((prev_high - prev_low) * Decimal((1.1 / 2))) + prev_close
        H5 = ((prev_high - prev_low) * Decimal((1.1 / 1.41))) + prev_close
        H6 = ((prev_high - prev_low) * Decimal((1.1 / 1.09))) + prev_close

        arr.extend((H6, H5, H4, H3, H2, H1, L1, L2, L3, L4, L5, L6))
        arr.reverse()
        buy_above_index = 0
        sell_below_index = 0
        for i in range(0, 10):
            # print( str(curr_open) + ' arr[i] : ' + str(arr[i]))
            if (curr_open >= arr[i] and curr_open <= arr[i + 1]):
                buy_above_index = i + 2
                sell_below_index = i - 1
                break

        retDict = dict();
        buy_above_val = arr[buy_above_index]
        sell_below_val = arr[sell_below_index]

        retDict['BUY_ABOVE'] = buy_above_val
        retDict['SELL_BELOW'] = sell_below_val

        merge_buy = False
        merge_sell = False
        if buy_above_index + 3 <= len(arr) - 1:
            merge_buy = True
            retDict['BTARGET1'] = arr[buy_above_index + 1]
            retDict['BTARGET2'] = arr[buy_above_index + 2]
            retDict['BTARGET3'] = arr[buy_above_index + 3]
            retDict['BSTOPLOSS'] = arr[sell_below_index]

        if sell_below_index - 3 >= 0:
            merge_sell = True
            retDict['STARGET1'] = arr[sell_below_index - 1]
            retDict['STARGET2'] = arr[sell_below_index - 2]
            retDict['STARGET3'] = arr[sell_below_index - 3]
            retDict['SSTOPLOSS'] = arr[buy_above_index]

        if not merge_buy:
            print(' @@@ PREDICTION_HELPER @@@ PLEASE CHECK STOCK')
            DTP = (prev_high + prev_low + prev_close) / 3
            S1 = 2 * DTP - prev_high
            R1 = 2 * DTP - prev_low
            S2 = DTP - (R1 - S1)
            R2 = DTP + (R1 - S1)
            S3 = prev_low - 2 * (prev_high - DTP)
            R3 = prev_high + 2 * (DTP - prev_low)

            r_arr = array('d')
            r_arr.extend([R1, R2, R3])

            buy_targets = array('d')
            buy_targets.extend(arr[buy_above_index:len(arr)])
            buy_targets.extend(r_arr)
            buy_targets_sorted = numpy.array(buy_targets)

            # Should be sorted in ascending order, I think.
            buy_targets_sorted.sort()
            # buy_targets_sorted[::-1].sort()

            btarget_counter = 0

            target_counter = 0
            for x in buy_targets_sorted:
                if x > buy_above_val and target_counter < 3:
                    retDict['BTARGET' + str((target_counter + 1))] = x
                    target_counter += 1
                    btarget_counter += 1

            if target_counter < 3:
                for i in range(3 - target_counter, 0, -1):
                    btarget_counter += 1
                    retDict['BTARGET' + str(btarget_counter)] = 0.0

            # for x in range(3,btarget_counter,-1):
            #     retDict['BTARGET' + str(x)]

            # retDict['BTARGET1'] = buy_targets_sorted[0]
            # retDict['BTARGET2'] = buy_targets_sorted[1]
            # retDict['BTARGET3'] = buy_targets_sorted[2]
            retDict['BSTOPLOSS'] = sell_below_val

        if not merge_sell:
            print(' @@@ PREDICTION_HELPER @@@ PLEASE CHECK SELL STOCK')
            DTP = (prev_high + prev_low + prev_close) / 3
            S1 = 2 * DTP - prev_high
            R1 = 2 * DTP - prev_low
            S2 = DTP - (R1 - S1)
            R2 = DTP + (R1 - S1)
            S3 = prev_low - 2 * (prev_high - DTP)
            R3 = prev_high + 2 * (DTP - prev_low)

            s_arr = array('d')
            # r_arr = array('d')

            s_arr.extend([S1, S2, S3])
            # r_arr.extend([R1, R2, R3])

            # buy_targets = array('d')
            sell_targets = array('d')
            #
            # buy_targets.extend(arr[buy_above_index:len(arr)])
            # buy_targets.extend(r_arr)
            # buy_targets_sorted = numpy.array(buy_targets)
            # buy_targets_sorted.sort()

            sell_targets.extend(arr[0:sell_below_index])
            sell_targets.extend(s_arr)
            sell_targets_sorted = numpy.array(sell_targets)
            sell_targets_sorted[::-1].sort()

            retDict['STARGET1'] = sell_targets_sorted[0]
            retDict['STARGET2'] = sell_targets_sorted[1]
            retDict['STARGET3'] = sell_targets_sorted[2]
            retDict['SSTOPLOSS'] = arr[buy_above_index]

        return retDict

    return retDict


