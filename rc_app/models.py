from django.db import models

# Create your models here.
from django.db.models.fields import DateTimeField


class Prediction(models.Model):
        id = models.AutoField(primary_key=True)
        symbol = models.CharField(max_length=30,null=True)
        exchange = models.CharField(max_length=10, null=True)
        type = models.CharField(max_length=10, null=True)
        prediction = models.CharField(max_length=10,null=True)
        target1 = models.DecimalField(decimal_places=2,max_digits=7)
        target2 = models.DecimalField(decimal_places=2,max_digits=7)
        target3 = models.DecimalField(decimal_places=2,max_digits=7)
        stoploss = models.DecimalField(decimal_places=2,max_digits=7)
        update_time = models.DateTimeField(auto_now_add=True)
        update_by = models.CharField(max_length=20)
        class Meta:
            db_table = 'PREDICTION'

class LiveSignal(models.Model):
    id = models.AutoField(primary_key=True)
    symbol = models.CharField(max_length=30)
    buy_above = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    btarget1 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    btarget2 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    btarget3 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    bstoploss = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    sell_below = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    starget1 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    starget2 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    starget3 = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    sstoploss = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    update_time = DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=20, null=True)
    class Meta:
        db_table = 'LIVESIGNAL'

class MarketData(models.Model):
    id = models.AutoField(primary_key=True)
    symbol = models.CharField(max_length=30)
    ltp = models.DecimalField(decimal_places=3,max_digits=8,null=True)
    open = models.DecimalField(decimal_places=3, max_digits=8, null=True)
    high = models.DecimalField(decimal_places=3, max_digits=8,null=True)
    low = models.DecimalField(decimal_places=3, max_digits=8,null=True,)
    prev_close= models.DecimalField(decimal_places=3, max_digits=8,null=True)
    atp = models.DecimalField(decimal_places=3, max_digits=8,null=True)
    update_time = DateTimeField(auto_now=True)
    created_by = models.CharField(max_length=20,null=True)
    class Meta:
        db_table = 'MARKETDATA'