import datetime
from _decimal import Decimal

import pythoncom
from win32com.client import Dispatch

from rc_app.models import MarketData


def loginFirst(zk):
    x = zk.cmdLogin(True, True)


def login(zk):
    init(zk)
    x = zk.cmdLogin(False, False)
    print(x)


def getSnapDataForSymbol(symbol_csv):
    zk = dispatch()
    login(zk)

    snap_data_all = zk.cmdSubscribe(symbol_csv)
    print('Got snap data for these symbols : \n' + symbol_csv)
    snap_dict = getDictForSnapDataResp(snap_data_all)
    return snap_dict


def getDictForSnapDataResp(snap_data_all):
    snap_data_split = snap_data_all.splitlines()
    retdict = dict()

    for el in snap_data_split:
        if el == 'OK':
            continue
        elif el == '':
            continue
        else:
            el_split = el.split(',')
            retdict[el_split[0]] = el_split[1]
    return retdict


def getATPFromSnapDataResp(snap_data_all):
    snap_data_split = snap_data_all.splitlines()
    retdict = dict()

    for el in snap_data_split:
        if el == 'OK':
            continue
        elif el == '':
            continue
        elif '.ATP' in el:
            el_split = el.split(',')
            retdict[el_split[0]] = Decimal(el_split[1])
    return retdict


def init(zk):
    zk.cmdInit("True1255", "shariq1255", "49.248.42.117")
    return zk


def dispatch():
    #Adding CoInitialize to avoid "CoInit has not been called error.
    pythoncom.CoInitialize()
    SERVICE = Dispatch("easyconnect.serverside")
    return  SERVICE


def getLiveDataAndInsertIntoDB(symbol,createdby):
    currentDataForAllTokens = getSnapDataForSymbol(symbol)

    splitted = symbol.split(",")

    for currToken in splitted:
        if len(currToken) > 0:
            print('For Symbol : ' + currToken)
            open = currentDataForAllTokens[currToken + '.DOP']
            high = currentDataForAllTokens[currToken + '.DHI']
            low = currentDataForAllTokens[currToken + '.DLO']
            atp =  currentDataForAllTokens[currToken+'.ATP']
            prev_close = currentDataForAllTokens[currToken+'.PCL']
            current = currentDataForAllTokens[currToken + '.LTP']

            print(current + "\n"  + open + "\n" + high + "\n" + low + "\n" + prev_close + "\n" + atp +"\n" )

            dbmodel = MarketData(
                symbol=currToken,
                current = current,
                open=open,
                high=high,
                low=low,
                atp0930=atp,
                created_by=createdby,
                prev_close = prev_close
            )
            dbmodel.save()
            print('Inserted Snap Data for ' + currToken)
    # Start real time data thread


def getLastEODsDict(symbol_list):
    print('@@ inside getLastEODsDict @@')
    zk = dispatch()
    init(zk)
    login(zk)

    retdict = dict()
    symbol_split = symbol_list.split(",")
    for curr_symbol in symbol_split:
        cur_dict = dict()
        resp = zk.cmdEodNumDays(curr_symbol, 2)
        #resp_split = resp.split(",") Please uncomment this and comment the below line if you change 2 to 1 in above live.
        resp_split = resp.split("\n")[1].split(",") #comment this
        cur_dict['OPEN'] = resp_split[1]
        cur_dict['HIGH'] = resp_split[2]
        cur_dict['LOW'] = resp_split[3]
        cur_dict['CLOSE'] = resp_split[4]
        retdict[curr_symbol] = cur_dict
    return retdict


def get_dict_from_Tlines_resp(stock_list):
    zk = dispatch()
    login(zk)
    zk.cmdSubscribe(stock_list)
    tlines_resp = zk.cmdTLine()
    return_dict = dict();

    for line in tlines_resp.splitlines():
        temp_dict = dict()
        line = line[0:len(line)-1]
        muzammil = line.split(",")
        symbol = muzammil[1]
        current = muzammil[2]
        open  = muzammil [10]
        #TODO: Add other resp values to dict .
        temp_dict['OPEN']  = Decimal(open)
        return_dict[symbol] = temp_dict

    return return_dict

def getOHLCFor0930(stock_list):
    zk = dispatch()
    login(zk)
    zk.cmdSubscribe(stock_list)
    return_dict = dict();

    for stock in stock_list.split(","):
        resp = zk.cmdGetMinData(stock, '2', '15')
        for line in resp.split('\n'):
            if "No data" in line:
                continue
            datepart = line[0:14]
            try:
                parseddate = datetime.datetime.strptime(datepart, "%Y%m%d %H:%M")
            except:
                continue
            if (parseddate.hour == 9 and parseddate.minute == 30) or (parseddate.hour == 9 and parseddate.minute > 30):
                splitted_lines = line.split(",")
                cur_dict = dict()
                cur_dict['OPEN'] = splitted_lines[1]
                cur_dict['HIGH'] = splitted_lines[2]
                cur_dict['LOW'] = splitted_lines[3]
                cur_dict['CLOSE'] = splitted_lines[4]
                return_dict[stock] = cur_dict
                break
    return return_dict

if __name__ == "__main__":
       # data_dict = getLiveDataAndInsertIntoDB("NIFTY-I,ACC-I",'0930')
        print(getOHLCFor0930("NIFTY-I,ACC-I"))


# Variables

# ServerStatus
# ValidTill
# SymbolLimit
# Exchanges
# AppStatus
#

# Commands

# cmdInit
# cmdLogin
# cmdLogout
# cmdSubscribe
# cmdUnsubscribe
# cmdUnsubAll
#
# cmdMdata
#
# cmdIntraTick
# cmdIntraTickNumDays
#
# cmdIntraMin
# cmdIntraMinNumDays
# cmdIntraMinCalDays
#
# cmdEODNumDays
# cmdEODCalDays
#
# Get Intraday Minute data
# print(zk.cmdIntraMin("NIFTY-I"))
